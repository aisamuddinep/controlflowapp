package id.co.iconpln.controlflowapp.fragmentViewPager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import id.co.iconpln.controlflowapp.fragmentTab.FirstFragment
import id.co.iconpln.controlflowapp.fragmentTab.SecondFragment

class ScrollPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){

    companion object{
        private const val NUM_PAGES = 3
    }

    override fun getItem(position: Int): Fragment {
        val fragment: Fragment? = when (position) {
            0 -> FirstFragment()
            1 -> SecondFragment()
            2 -> ScrollFragment()
            else -> null
        }
        return fragment as Fragment
    }

    override fun getCount(): Int {
        return NUM_PAGES
    }

}