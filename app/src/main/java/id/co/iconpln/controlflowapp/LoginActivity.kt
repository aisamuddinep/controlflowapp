package id.co.iconpln.controlflowapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Patterns.EMAIL_ADDRESS
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnLogin.setOnClickListener {
            checkField()
        }
    }


    private fun checkField() {
        val username = etLoginUsername.text.toString()
        val password = etLoginPassword.text.toString()

        val validUsername = if (checkBlankField(etLoginUsername)) {
            isEmailValid(etLoginUsername)
        } else false

        val validPassword = if (checkBlankField(etLoginPassword)) {
            checkMinValue(7, etLoginPassword)
        } else false

        if (validUsername && validPassword) checkLogin(username, password)
    }

    private fun checkMinValue(limit: Int, editText: EditText): Boolean {
        return if (editText.text.length < limit) {
            editText.error = "${editText.hint} must be more or equal to $limit"
            false
        } else true
    }

    private fun checkBlankField(editText: EditText): Boolean {
        return if (editText.text.isBlank()) {
            editText.error = "${editText.hint} can't be blank"
            false
        } else true
    }


    @SuppressLint("SetTextI18n")
    private fun checkLogin(username: String, password: String) {
        val msg: String = if (username == "user@mail.com" && password == "password")
            "Success"
        else "Failed"
        tvLoginStatus.text = "Status Login: $msg"
    }

    private fun isEmailValid(editText: EditText): Boolean {
        return if (EMAIL_ADDRESS.matcher(editText.text.toString()).matches()) {
            true
        } else {
            editText.error = "${editText.hint} wrong format"
            false
        }
    }
}
