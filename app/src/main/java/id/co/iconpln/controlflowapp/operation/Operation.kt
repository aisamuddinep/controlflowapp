package id.co.iconpln.controlflowapp.operation

sealed class Operation {
    class Add(val value: Float) : Operation()
    class Divide(val value: Float) : Operation()
    class Multiply(val value: Float) : Operation()
    class Subtract(val value: Float) : Operation()
    
}
