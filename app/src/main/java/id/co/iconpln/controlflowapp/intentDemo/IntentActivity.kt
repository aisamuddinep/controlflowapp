package id.co.iconpln.controlflowapp.intentDemo

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.*
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent.*

class IntentActivity : AppCompatActivity(), View.OnClickListener {

    private val REQUEST_CODE = 110

    private val btnIntentMove: Button
        get() = btn_intent_move

    private val btnIntentMoveWithData: Button
        get() = btn_intent_move_with_data

    private val btnIntentMoveWithBundle: Button
        get() = btn_intent_move_with_bundle

    private val btnIntentMoveWithObject: Button
        get() = btn_intent_move_with_object

    private val btnIntentImplicit: Button
        get() = btn_intent_implicit

    private val btnIntentWithResult: Button
        get() = btn_intent_with_result

    private val btnIntentOpenWeb: Button
        get() = btn_intent_open_web

    private val btnIntentSendSms: Button
        get() = btn_intent_send_sms

    private val btnIntentShowMap: Button
        get() = btn_intent_show_map

    private val btnIntentShareText: Button
        get() = btn_intent_share_text

    private val tvIntentResult: TextView
        get() = tv_intent_result

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        setOnClickListener()
    }

    private fun setOnClickListener() {
        btnIntentMove.setOnClickListener(this)
        btnIntentMoveWithBundle.setOnClickListener(this)
        btnIntentMoveWithData.setOnClickListener(this)
        btnIntentMoveWithObject.setOnClickListener(this)
        btnIntentImplicit.setOnClickListener(this)
        btnIntentWithResult.setOnClickListener(this)
        btnIntentOpenWeb.setOnClickListener(this)
        btnIntentSendSms.setOnClickListener(this)
        btnIntentShowMap.setOnClickListener(this)
        btnIntentShareText.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            btnIntentMove.id -> {
                val moveIntent = Intent(this, StyleActivity::class.java)
                startActivity(moveIntent)
            }
            btnIntentMoveWithData.id -> {
                val intentMoveDataIntent = Intent(this, IntentMoveDataActivity::class.java)
                intentMoveDataIntent.putExtra(IntentMoveDataActivity.EXTRA_NAME, "Isam")
                intentMoveDataIntent.putExtra(IntentMoveDataActivity.EXTRA_AGE, 22)
                startActivity(intentMoveDataIntent)
            }
            btnIntentMoveWithBundle.id -> {
                val bundle = Bundle()
                bundle.putString(IntentMoveBundleActivity.EXTRA_BUNDLE_NAME, "Isam")
                bundle.putInt(IntentMoveBundleActivity.EXTRA_BUNDLE_AGE, 17)

                val intentMoveBundleIntent = Intent(this, IntentMoveBundleActivity::class.java)
                intentMoveBundleIntent.putExtras(bundle)
                startActivity(intentMoveBundleIntent)
            }
            btnIntentMoveWithObject.id -> {
                val person = Person(
                    "Isam",
                    17,
                    "isam@mail.com",
                    "Malang"
                )
                val intentMoveBundleIntent = Intent(this, IntentMoveObjectActivity::class.java)
                intentMoveBundleIntent.putExtra(IntentMoveObjectActivity.EXTRA_PERSON, person)
                startActivity(intentMoveBundleIntent)
            }
            btnIntentImplicit.id -> {
                val phoneNumber = "082257307777"
                val dialPhoneIntent = Intent(
                    Intent.ACTION_DIAL,
                    Uri.parse("tel: $phoneNumber")
                )
                if (dialPhoneIntent.resolveActivity(packageManager) != null) {
                    startActivity(dialPhoneIntent)
                }
            }
            btnIntentWithResult.id -> {
                val intentWithResultIntent = Intent(this, IntentWithResultActivity::class.java)
                startActivityForResult(intentWithResultIntent, REQUEST_CODE)
            }
            btnIntentOpenWeb.id -> {
                val webPage = Uri.parse("https://www.binar.co.id")
                val openWebIntent = Intent(Intent.ACTION_VIEW, webPage)
                if (openWebIntent.resolveActivity(packageManager) != null) {
                    startActivity(openWebIntent)
                }
            }
            btnIntentSendSms.id -> {
                val phoneNumber = "082257307777"
                val sendSms = Uri.parse("sms: $phoneNumber")
                val message = "Hallo, leh nal ga?"

                val sendSmsIntent = Intent(Intent.ACTION_SENDTO, sendSms)
                sendSmsIntent.putExtra("sms_body", message)

                if (sendSmsIntent.resolveActivity(packageManager) != null) {
                    startActivity(sendSmsIntent)
                }
            }
            btnIntentShowMap.id -> {
                val latitude = "47.6"
                val longitude = "-122.3"
                val showMap = Uri.parse("geo: $latitude, $longitude")

                val showMapIntent = Intent(Intent.ACTION_VIEW, showMap)

                if (showMapIntent.resolveActivity(packageManager) != null){
                    startActivity(showMapIntent)
                }
            }
            btnIntentShareText.id -> {
                val sharedText = "Ini teks yang di share"

                val shareTextIntent = Intent(Intent.ACTION_SEND)
                shareTextIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
                shareTextIntent.type = "text/plain"
                
                val shareIntent = Intent.createChooser(shareTextIntent, "Share Aja")
                if (shareIntent.resolveActivity(packageManager) != null) startActivity(shareIntent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == IntentWithResultActivity.RESULT_CODE) {
                val selectedValue = data?.getIntExtra(IntentWithResultActivity.EXTRA_VALUE, 0)
                tvIntentResult.text = "Hasil = $selectedValue"
            }
        }
    }
}
