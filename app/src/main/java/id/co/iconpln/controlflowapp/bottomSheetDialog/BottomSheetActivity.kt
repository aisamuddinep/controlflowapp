package id.co.iconpln.controlflowapp.bottomSheetDialog

import android.opengl.Visibility
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_bottom_sheet.view.*
import kotlinx.android.synthetic.main.layout_bottom_sheet.*
import kotlinx.android.synthetic.main.layout_content_main.*

class BottomSheetActivity : AppCompatActivity(), View.OnClickListener,
    BottomSheetFragment.ItemClickListener {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_sheet)

        setupActionBar()
        setOnClickListener()
        setupBottomSheetBehavior()
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbarBottomSheet)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setOnClickListener() {
        btnBottomSheet.setOnClickListener(this)
        btnBottomSheetDialog.setOnClickListener(this)
        btnBottomSheetDialogFragment.setOnClickListener(this)
        btnBottomSheetPayment.setOnClickListener(this)
    }

    private fun setupBottomSheetBehavior() {

        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)
        bottomSheetBehavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.setBottomSheetCallback(
            object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(view: View, slideOffset: Float) {

                }

                override fun onStateChanged(view: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_COLLAPSED -> {
                            btnBottomSheet.text = "Expand Bottom Sheet"
                            showAppBar(ablBottomSheet)
                        }
                        BottomSheetBehavior.STATE_DRAGGING -> {
                        }
                        BottomSheetBehavior.STATE_EXPANDED -> {
                            btnBottomSheet.text = "Close Bottom Sheet"
                            hideAppBar(ablBottomSheet)
                        }
                        BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                            btnBottomSheet.text = "Show Bottom Sheet"
                        }
                        BottomSheetBehavior.STATE_HIDDEN -> {
                        }
                        BottomSheetBehavior.STATE_SETTLING -> {
                        }
                    }
                }
            }
        )
    }

    private fun showAppBar(view: View) {
        /*val params = view.layoutParams
        params.height = android.R.attr.actionBarSize
        view.layoutParams = params*/
        view.visibility = View.VISIBLE
    }

    private fun hideAppBar(view: View) {
        /*val params = view.layoutParams
        params.height = 0
        view.layoutParams = params*/
        view.visibility = View.GONE
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> false
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnBottomSheet -> {
                bottomSheetBehavior.state = when (bottomSheetBehavior.state) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        BottomSheetBehavior.STATE_EXPANDED
                    }
                    BottomSheetBehavior.STATE_HIDDEN -> BottomSheetBehavior.STATE_COLLAPSED
                    else -> BottomSheetBehavior.STATE_COLLAPSED
                }
            }
            R.id.btnBottomSheetDialog -> {
                val dialogView = layoutInflater.inflate(R.layout.fragment_bottom_sheet, null)
                val bottomSheetDialog = BottomSheetDialog(this)

                dialogSetOnClickListener(dialogView.llBottomPreview, dialogView.tvBottomPreview)
                dialogSetOnClickListener(dialogView.llBottomShare, dialogView.tvBottomShare)
                dialogSetOnClickListener(dialogView.llBottomEdit, dialogView.tvBottomEdit)
                dialogSetOnClickListener(dialogView.llBottomSearch, dialogView.tvBottomSearch)
                dialogSetOnClickListener(dialogView.llBottomExit, dialogView.tvBottomExit)

                bottomSheetDialog.setContentView(dialogView)
                bottomSheetDialog.show()
            }
            R.id.btnBottomSheetDialogFragment -> {
                val bottomSheetFragment = BottomSheetFragment()
                bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.tag)
            }
            btnBottomSheetPayment.id -> tvBottomActivity.text = "Order is Paid"
        }
    }

    private fun dialogSetOnClickListener(linearLayout: LinearLayout, textView: TextView) {
        linearLayout.setOnClickListener { onItemClick("Dialog ${textView.text}") }
    }

    override fun onItemClick(text: String) {
        tvBottomActivity.text = text
    }
}
