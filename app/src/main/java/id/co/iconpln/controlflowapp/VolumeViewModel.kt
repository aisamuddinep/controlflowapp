package id.co.iconpln.controlflowapp

import androidx.lifecycle.ViewModel

class VolumeViewModel : ViewModel(){

    var volumeResult: Double = 0.0

    fun calculate(length: String, width: String, height: String) {
        volumeResult = length.toDouble() * width.toDouble() * height.toDouble()
    }
}