package id.co.iconpln.controlflowapp.hero

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import id.co.iconpln.controlflowapp.model.HeroesData
import kotlinx.android.synthetic.main.activity_grid_hero.*

class GridHeroActivity : AppCompatActivity() {

    private val listHero : ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_hero)

        setupListHero()
    }

    private fun setupListHero() {
        rvGridListHero.setHasFixedSize(true)
        listHero.addAll(HeroesData.listDataHero)

        showRecyclerList()
        setupDivider()
    }

    private fun setupDivider() {
        rvGridListHero.addItemDecoration(DividerItemDecoration(rvGridListHero.context, DividerItemDecoration.HORIZONTAL))
        rvGridListHero.addItemDecoration(DividerItemDecoration(rvGridListHero.context, DividerItemDecoration.VERTICAL))
    }

    private fun showRecyclerList() {
        rvGridListHero.layoutManager = GridLayoutManager(this, 2)
        val adapter = GridHeroAdapter(listHero)
        rvGridListHero.adapter = adapter

        adapter.setOnItemClickCallBack(object : GridHeroAdapter.OnItemClickCallBack{
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@GridHeroActivity, hero.name, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
