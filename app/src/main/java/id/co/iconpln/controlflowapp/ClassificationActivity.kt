package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_classification.*
import kotlinx.android.synthetic.main.activity_main.*

class ClassificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classification)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        etClassificationNilai.setText("0")

        btnClassificationShow.setOnClickListener {
            checkField()

        }
    }

    private fun checkField() {
        val alertClassification = AlertDialog.Builder(this)
        alertClassification.setTitle("Classification Alert")
        alertClassification.setPositiveButton(android.R.string.yes) { _, _ ->
            Toast.makeText(applicationContext,
                android.R.string.yes, Toast.LENGTH_SHORT).show()
        }

        when (val value = etClassificationNilai.text.toString().toIntOrNull()){
            null -> {
                alertClassification.setMessage("Please input number")
                alertClassification.show()
            }
            !in 0..1000 -> {
                alertClassification.setMessage("Max input number 1000")
                alertClassification.show()
            }
            else -> {
                doClassification(value)
            }
        }
    }

    private fun doClassification(nilai: Int) {
        val msg: String = when (nilai){
            in 0..70 -> "Tidak Lulus"
            in 71..80 -> "Lulus Aja"
            in 81..100 -> "Lulus Banget"
            else -> "Input Error"
        }

        tvClassificationResult.text = "Hasilnya: $msg"
    }
}
