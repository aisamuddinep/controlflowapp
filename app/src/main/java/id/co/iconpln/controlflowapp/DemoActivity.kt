package id.co.iconpln.controlflowapp

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.isam.logutil.LogDebug
import kotlinx.android.synthetic.main.activity_demo.*

class DemoActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        LogDebug.d("On Create")

        setOnClickListener()
    }

    private fun setOnClickListener() {
        btnSubmit.setOnClickListener(this)
        btnSnackbar.setOnClickListener(this)
        btnSnackbarButton.setOnClickListener(this)
        btnSnackbarCustom.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnSubmit -> {
                val styleIntent = Intent(this, StyleActivity::class.java)
                startActivity(styleIntent)
            }
            R.id.btnSnackbar -> {
                Snackbar.make(clDemo, "This is Snackbar", Snackbar.LENGTH_SHORT).show()
            }
            R.id.btnSnackbarButton -> {
                Snackbar.make(clDemo, "Message is deleted", Snackbar.LENGTH_SHORT)
                    .setAction("Undo", undoListener)
                    .show()
            }
            R.id.btnSnackbarCustom -> {
                val customSnackbar =
                    Snackbar.make(clDemo, "This is a Custom Snackbar", Snackbar.LENGTH_SHORT)
                        .setAction("Undo", undoListener)
                        .setActionTextColor(
                            ContextCompat.getColor(
                                this,
                                R.color.colorButtonSnackbar
                            )
                        )
                val snackbarView = customSnackbar.view
                val textSnackbar: TextView = snackbarView.findViewById(R.id.snackbar_text)

                textSnackbar.apply {
                    setTextColor(ContextCompat.getColor(this@DemoActivity, R.color.colorAccent))
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
                }

                snackbarView.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorPrimaryDark
                    )
                )

                customSnackbar.show()
            }
        }
    }

    private val undoListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            Snackbar.make(clDemo, "Message is restored", Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_demo, menu)
        setupSearchView(menu)

        return super.onCreateOptionsMenu(menu)
    }

    private fun setupSearchView(menu: Menu?) {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.actionDemoSearch)?.actionView as SearchView

        searchView.apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            queryHint = getString(R.string.search_hint)
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    Toast.makeText(this@DemoActivity, query, Toast.LENGTH_SHORT).show()
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }
            })
        }
    }

    override fun onStart() {
        super.onStart()
        LogDebug.d("On Start")
    }

    override fun onResume() {
        super.onResume()
        LogDebug.d("On Resume")
    }

    override fun onPause() {
        super.onPause()
        LogDebug.d("On Pause")
    }

    override fun onStop() {
        super.onStop()
        LogDebug.d("On Stop")
    }

    override fun onRestart() {
        super.onRestart()
        LogDebug.d("On Restart")
    }

    override fun onDestroy() {
        LogDebug.d("On Destroy")
        super.onDestroy()
    }
}
