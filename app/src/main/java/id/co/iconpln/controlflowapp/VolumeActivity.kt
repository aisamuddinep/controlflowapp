package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_volume.*
import java.text.DecimalFormat

class VolumeActivity : AppCompatActivity() {

    private val etVolumeLength : EditText
        get() = et_volume_length

    private val etVolumeWidth : EditText
        get() = et_volume_width

    private val etVolumeHeight : EditText
        get() = et_volume_height

    private val tvVolumeResult : TextView
        get() = tv_volume_result

    private val btnVolumeCalculate : Button
        get() = btn_volume_calculate

    private lateinit var volumeViewModel: VolumeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)

        initViewModel()
        displayResult()
        setClickListener()
    }

    private fun initViewModel() {
        volumeViewModel = ViewModelProviders.of(this).get(VolumeViewModel::class.java)
    }

    private fun displayResult() {
        val df = DecimalFormat("###.###")
        tvVolumeResult.text = df.format(volumeViewModel.volumeResult)
    }

    private fun setClickListener() {
        btnVolumeCalculate.setOnClickListener{
            val length = etVolumeLength.text.toString()
            val width = etVolumeWidth.text.toString()
            val height = etVolumeHeight.text.toString()

            if (length.isEmpty()){
                etVolumeLength.error = "Empty Field"
            } else if (width.isEmpty()){
                etVolumeWidth.error = "Empty Field"
            } else if (height.isEmpty()) {
                etVolumeHeight.error = "Empty Field"
            } else {
                volumeViewModel.calculate(length,width, height)
                displayResult()
            }
        }
    }


}
