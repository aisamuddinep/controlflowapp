package id.co.iconpln.controlflowapp.fragmentTab

import android.content.Context
import androidx.annotation.Nullable
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.hero.ListHeroFragment

class TabPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    @StringRes
    private val TAB_TITLE = intArrayOf(R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3)

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = FirstFragment()
            1 -> fragment = SecondFragment()
            2 -> fragment = ListHeroFragment()
        }
        return fragment as Fragment
    }

    override fun getCount(): Int {
        return 3
    }

    @Nullable
    override fun getPageTitle(position: Int): CharSequence? {
        //return super.getPageTitle(position)
        return context.resources.getString(TAB_TITLE[position])
    }
}