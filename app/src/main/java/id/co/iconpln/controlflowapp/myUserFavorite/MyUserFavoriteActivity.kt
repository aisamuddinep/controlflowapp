package id.co.iconpln.controlflowapp.myUserFavorite

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user_favorite.*

class MyUserFavoriteActivity : AppCompatActivity() {

    private lateinit var adapter: MyUserFavoriteAdapter

    private lateinit var favoriteViewModel: FavoriteViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_favorite)

        setupActionBar()
        initViewModel()
        showListMyUser()
        fetchFavoriteUserData()
        setOnItemClick()
    }

    override fun onResume() {
        super.onResume()
        favoriteViewModel.getAllFavoriteUser()
    }

    private fun setOnItemClick() {
        adapter.setOnItemClickCallback(object : MyUserFavoriteAdapter.OnItemClickCallback {
            override fun onItemClick(myUserFavItem: FavoriteUser) {
                openUserForm(myUserFavItem)
            }
        })
    }

    private fun openUserForm(myUserFavItem: FavoriteUser) {
        val myUserFormIntent = Intent(this@MyUserFavoriteActivity, MyUserFormActivity::class.java)
        myUserFormIntent.putExtra(MyUserFormActivity.EXTRA_MY_USER_ID, myUserFavItem.userId.toInt())
        myUserFormIntent.putExtra(MyUserFormActivity.EXTRA_MY_USER_EDIT, true)
        startActivity(myUserFormIntent)
    }

    private fun setupActionBar() {
        supportActionBar?.title = "Favorite User List"
    }

    private fun initViewModel() {
        favoriteViewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory(application)
        ).get(FavoriteViewModel::class.java)
    }

    private fun showListMyUser() {
        adapter = MyUserFavoriteAdapter()
        adapter.notifyDataSetChanged()

        rvMyUserFavorite.layoutManager = LinearLayoutManager(this)
        rvMyUserFavorite.adapter = adapter
    }

    private fun fetchFavoriteUserData(){
        favoriteViewModel.getAllFavoriteUser().observe(this, Observer {
            listFavUser -> adapter.setData(listFavUser)
        })
    }
}
