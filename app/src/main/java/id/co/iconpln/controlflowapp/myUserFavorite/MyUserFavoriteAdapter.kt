package id.co.iconpln.controlflowapp.myUserFavorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import kotlinx.android.synthetic.main.item_list_my_user.view.*

class MyUserFavoriteAdapter: RecyclerView.Adapter<MyUserFavoriteAdapter.MyUserFavoriteViewHolder>(){
    private var myUserFavoriteData = emptyList<FavoriteUser>()
    private lateinit var onItemClickCallback: OnItemClickCallback


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyUserFavoriteViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_my_user, parent, false)
        return MyUserFavoriteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myUserFavoriteData.size
    }

    override fun onBindViewHolder(holder: MyUserFavoriteViewHolder, position: Int) {
        holder.bind(myUserFavoriteData[position])
        holder.itemView.setOnClickListener{
            onItemClickCallback.onItemClick(myUserFavoriteData[holder.adapterPosition])
        }
    }

    fun setData(myUserFavItem: List<FavoriteUser>){
        val listFavUser = ArrayList<FavoriteUser>()
        myUserFavItem.forEach{favUser ->
            listFavUser.add(favUser)
        }
        myUserFavoriteData = listFavUser
        notifyDataSetChanged()
    }

    inner class MyUserFavoriteViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(myUserItem: FavoriteUser) {
            itemView.tvMyUserName.text = myUserItem.userName
            itemView.tvMyUserAddress.text = myUserItem.userAddress
            itemView.tvMyUserPhone.text = myUserItem.userPhone
        }
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback{
        fun onItemClick(myUserFavItem: FavoriteUser)
    }
}