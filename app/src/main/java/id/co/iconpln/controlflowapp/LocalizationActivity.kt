package id.co.iconpln.controlflowapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_localization.*

class LocalizationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_localization)

        displayLocalizedText()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_localization, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_language) {
            val settingIntent = Intent(Settings.ACTION_LOCALE_SETTINGS)
            startActivity(settingIntent)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun displayLocalizedText() {
        val pokeCount = 3
        val hello = resources.getString(R.string.locale_count_hello, "IconPlus", pokeCount, "Isam")
        tvLocalHello.text = hello

        val songCount = 5
        val pluralText = resources.getQuantityString(R.plurals.locale_count_plural_songs, songCount, songCount)
        tvLocalPlural.text = pluralText
    }
}
