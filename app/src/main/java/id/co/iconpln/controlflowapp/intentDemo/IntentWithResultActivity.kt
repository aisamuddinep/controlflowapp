package id.co.iconpln.controlflowapp.intentDemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RadioGroup
import android.widget.TextView
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_intent_with_result.*

class IntentWithResultActivity : AppCompatActivity(), View.OnClickListener {

    private val tvResultData : TextView
        get() = tv_result_data

    private val rgNumber : RadioGroup
        get() = rg_number

    private val btnResultSelect : Button
        get() = btn_result_select

    companion object{
        const val TAG = "IntentWithResult"
        const val EXTRA_VALUE = "extra_value"
        const val RESULT_CODE = 110
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_with_result)

        setOnClickListener()
    }

    private fun setOnClickListener() {
        btnResultSelect.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (view.id == btnResultSelect.id){
            if(rgNumber.checkedRadioButtonId != 0){
                var value = 0
                when (rgNumber.checkedRadioButtonId){
                    R.id.rb_50 -> value = 50
                    R.id.rb_100 -> value = 100
                    R.id.rb_150 -> value = 150
                    R.id.rb_200 -> value = 200
                }
                Log.d(TAG,"Value = $value")

                val resultIntent = Intent()
                resultIntent.putExtra(EXTRA_VALUE, value)
                setResult(RESULT_CODE, resultIntent)
                finish()
            }
        }
    }

}
