package id.co.iconpln.controlflowapp.intentDemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent_move_object.*

class IntentMoveObjectActivity : AppCompatActivity() {

    private val tvObjectReceive : TextView
        get() = tv_object_receive

    companion object{
        const val EXTRA_PERSON = "extra_person"
    }

    private var person: Person? =
        Person()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_move_object)

        getIntentExtras()
        showData()
    }

    private fun getIntentExtras() {
        person = intent.getParcelableExtra(EXTRA_PERSON)
    }

    private fun showData() {
        val text = "Name: ${person?.name}, \nAge: ${person?.age}, \nEmail: ${person?.email}, \nCity: ${person?.city}."
        tvObjectReceive.text = text
    }
}
