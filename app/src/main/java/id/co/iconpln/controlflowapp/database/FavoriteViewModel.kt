package id.co.iconpln.controlflowapp.database

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FavoriteViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: FavoriteDatabaseRepository

    private val allFavoriteUsers: LiveData<List<FavoriteUser>>

    init {
        val favDatabaseDao = FavoriteDatabase.getInstance(application).favoriteDatabaseDao
        repository = FavoriteDatabaseRepository(favDatabaseDao)
        allFavoriteUsers = repository.allFavUsers
    }

    fun getAllFavoriteUser(): LiveData<List<FavoriteUser>> {
        return allFavoriteUsers
    }

    fun insertUser(user: FavoriteUser) {
        GlobalScope.launch {
            repository.insertUser(user)
            Log.d("Isam ", "User : ${user.userName}, Id: ${user.userId} - - INSERTED")
        }
    }
    fun deleteUser(userId: Int) {
        GlobalScope.launch {
            repository.deleteUser(userId)
            Log.d("Isam ", "$userId - - DELETED")
        }
    }

    fun getUser(userId: Int): LiveData<FavoriteUser>{
        return repository.getUser(userId)
    }

    fun updateUser(user: FavoriteUser){
        GlobalScope.launch {
            repository.updateUser(user)
            Log.d("Isam", "Username: ${user.userName} - ID: ${user.userId} - - UPDATED")
        }
    }
}