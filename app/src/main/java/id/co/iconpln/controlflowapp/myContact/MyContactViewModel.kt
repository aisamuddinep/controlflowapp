package id.co.iconpln.controlflowapp.myContact

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import id.co.iconpln.controlflowapp.contact.Contact
import id.co.iconpln.controlflowapp.model.myContact.ContactResponse
import id.co.iconpln.controlflowapp.network.MyContactNetworkRepository
import org.json.JSONObject
import java.lang.Exception

class MyContactViewModel : ViewModel(){

    fun getListContact(): MutableLiveData<ArrayList<ContactResponse>>{
        return MyContactNetworkRepository().fetchContact()
    }
}