package id.co.iconpln.controlflowapp.contactFragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.contact.Contact
import kotlinx.android.synthetic.main.item_list_contact.view.*

class ContactAdapterFragment :
    RecyclerView.Adapter<ContactAdapterFragment.ContactFragmentViewHolder>() {
    private val contactData = ArrayList<Contact>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactFragmentViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_contact, parent, false)
        return ContactFragmentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contactData.size
    }

    override fun onBindViewHolder(holder: ContactFragmentViewHolder, position: Int) {
        holder.bind(contactData[position])
    }

    fun setData(contactItem: ArrayList<Contact>){
        contactData.clear()
        contactData.addAll(contactItem)
        notifyDataSetChanged()
    }

    inner class ContactFragmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(contactItem: Contact) {
            itemView.tvContactName.text = contactItem.nama
            itemView.tvContactEmail.text = contactItem.email
            itemView.tvContactMobile.text = contactItem.mobile
        }
    }
}