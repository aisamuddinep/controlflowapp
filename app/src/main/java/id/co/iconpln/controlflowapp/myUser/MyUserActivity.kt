package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.myUserFavorite.MyUserFavoriteActivity
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user.*

class MyUserActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var adapter: MyUserAdapter
    private lateinit var myUserViewModel: MyUserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user)

        initViewModel()
        showMyUserList()

        fetchMyUserData()
        setOnItemClick()
        setClickListener()
    }

    override fun onResume() {
        super.onResume()
        fetchMyUserData()
    }

    private fun initViewModel() {
        myUserViewModel = ViewModelProvider(this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyUserViewModel::class.java)
    }

    private fun showMyUserList() {
        adapter = MyUserAdapter()
        adapter.notifyDataSetChanged()

        rvMyUserList.layoutManager = LinearLayoutManager(this)
        rvMyUserList.adapter = adapter
    }

    private fun fetchMyUserData() {
        myUserViewModel.getListUser().observe(this, Observer { myUserItem ->
            if (myUserItem != null){
                adapter.setData(myUserItem)
                showLoading(false)
            }
        })
    }

    private fun showLoading(state: Boolean) {
        if (state) {
            pbMyUser.visibility = View.VISIBLE
        } else {
            pbMyUser.visibility = View.GONE
        }
    }

    private fun setOnItemClick() {
        adapter.setOnItemClickCallback(object : MyUserAdapter.OnItemClickCallback{
            override fun onItemClick(myUser: UserDataResponse) {
                openUserForm(myUser)
            }
        })
    }

    private fun openUserForm(myUser: UserDataResponse) {
        val myUserFormIntent = Intent(this@MyUserActivity, MyUserFormActivity::class.java)
        myUserFormIntent.putExtra(MyUserFormActivity.EXTRA_MY_USER_ID, myUser.id)
        myUserFormIntent.putExtra(MyUserFormActivity.EXTRA_MY_USER_EDIT, true)
        startActivity(myUserFormIntent)
    }

    private fun setClickListener() {
        fabMyUserAdd.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.fabMyUserAdd -> startActivity(Intent(this, MyUserFormActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_open_favorite, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        startActivity(Intent(this, MyUserFavoriteActivity::class.java))
        return super.onOptionsItemSelected(item)
    }
}
