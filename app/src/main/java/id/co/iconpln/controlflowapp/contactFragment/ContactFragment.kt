package id.co.iconpln.controlflowapp.contactFragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_contact_tab.*
import kotlinx.android.synthetic.main.fragment_contact.*

/**
 * A simple [Fragment] subclass.
 */
class ContactFragment : Fragment() {

    private lateinit var contactViewModel: ContactViewModelFragment
    private lateinit var adapter: ContactAdapterFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        showContactList()
        contactViewModel.setContact()
        fetchContactData()
    }

    private fun fetchContactData() {
        contactViewModel.getContact().observe(this, Observer { contactItem ->
            if (contactItem != null) {
                adapter.setData(contactItem)
                showLoading(false)
            }
        })
    }

    private fun showLoading(state: Boolean) {
        if (state) {
            pbContactFragment.visibility = View.VISIBLE
        } else {
            pbContactFragment.visibility = View.GONE
        }
    }

    private fun initViewModel() {
        contactViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ContactViewModelFragment::class.java)
    }

    private fun showContactList() {
        adapter = ContactAdapterFragment()
        adapter.notifyDataSetChanged()

        rvFragmentContactList.layoutManager = LinearLayoutManager(requireContext())
        rvFragmentContactList.adapter = adapter
    }
}
