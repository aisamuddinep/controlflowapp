package id.co.iconpln.controlflowapp.intentDemo

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_intent_move_bundle.*

class IntentMoveBundleActivity : AppCompatActivity() {

    private val tvIntentBundleReceive: TextView
        get() = tv_intent_bundle_receive

    companion object {
        const val EXTRA_BUNDLE_NAME = "extra_bundle_name"
        const val EXTRA_BUNDLE_AGE = "extra_bundle_age"
    }

    private var name: String = ""
    private var age: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_move_bundle)

        getIntentExtra()
        showData()
    }

    private fun getIntentExtra() {
        val bundleReceive = intent.extras

        name = bundleReceive?.getString(EXTRA_BUNDLE_NAME) ?: ""
        age = bundleReceive?.getInt(EXTRA_BUNDLE_AGE) ?: 0
    }

    private fun showData() {
        val text = "Name: $name \nAge: $age"
        tvIntentBundleReceive.text = text
    }
}
