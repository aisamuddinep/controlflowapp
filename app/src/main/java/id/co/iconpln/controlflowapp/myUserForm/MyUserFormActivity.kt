package id.co.iconpln.controlflowapp.myUserForm

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.activity_my_user_form.*

class MyUserFormActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_MY_USER_ID = "extra_my_user_id"
        const val EXTRA_MY_USER_EDIT = "extra_my_user_edit"
    }

    private lateinit var myUserFormViewModel: MyUserFormViewModel
    private lateinit var favViewModel: FavoriteViewModel

    private var userId: Int? = null

    private var isEditUser = false

    private var menuItem: Menu? = null

    private var isFavorite = false

    private var favoriteUserId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_form)

        initViewModel()
        getIntentExtras()
        setOnClickListener()
        checkForm(isEditUser)
    }

    private fun initViewModel() {
        myUserFormViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())
            .get(MyUserFormViewModel::class.java)

        favViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application))
                .get(FavoriteViewModel::class.java)
    }

    private fun getIntentExtras() {
        userId = intent.getIntExtra(EXTRA_MY_USER_ID, 0)
        isEditUser = intent.getBooleanExtra(EXTRA_MY_USER_EDIT, false)
    }

    private fun setOnClickListener() {
        btnMyUserFormSave.setOnClickListener(this)
        btnMyUserFormDelete.setOnClickListener(this)
        btnMyUserFormAdd.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnMyUserFormSave -> userSaveData()
            R.id.btnMyUserFormAdd -> userSaveData()
            R.id.btnMyUserFormDelete -> deleteUser()
        }
    }

    private fun checkForm(editUser: Boolean) {
        if (editUser) {
            fetchUserData()
        } else {
            btnMyUserFormSave.visibility = View.GONE
            btnMyUserFormDelete.visibility = View.GONE
            btnMyUserFormAdd.visibility = View.VISIBLE
        }
    }

    private fun fetchUserData(){
        pbMyUserFormLoading.visibility = View.VISIBLE
        llMyUserFormContent.visibility = View.GONE
        getUser(userId as Int)
    }

    private fun populateData(user: UserDataResponse) {
        etMyUserName.setText(user.name)
        etMyUserAddress.setText(user.address)
        etMyUserPhone.setText(user.phone)
        btnMyUserFormSave.visibility = View.VISIBLE
        btnMyUserFormDelete.visibility = View.VISIBLE
        btnMyUserFormAdd.visibility = View.GONE
    }

    private fun userSaveData() {
        val name = etMyUserName.text.toString()
        val address: String = etMyUserAddress.text.toString()
        val phone: String = etMyUserPhone.text.toString()

        if (!validationField(name, address, phone)) return

        val userData = UserDataResponse(
            address,
            userId ?: 0,
            name,
            phone
        )

        pbMyUserFormLoading.visibility = View.VISIBLE

        if (userId != null && isEditUser) {
            updateUserData(userId as Int, userData)
        } else {
            createUserData(userData)
        }
    }

    private fun validationField(name: String, address: String, phone: String): Boolean {
        if (name.isEmpty()) {
            etMyUserName.error = resources.getString(R.string.sp_field_required)
            return false
        }
        if (address.isEmpty()) {
            etMyUserAddress.error = resources.getString(R.string.sp_field_required)
            return false
        }
        if (phone.isEmpty()) {
            etMyUserPhone.error = resources.getString(R.string.sp_field_required)
            return false
        }
        if (!isValidPhone(phone)) {
            etMyUserPhone.error = "Harus Nomor Telephone"
            return false
        }
        return true
    }

    private fun isValidPhone(phone: String): Boolean {
        return android.util.Patterns.PHONE.matcher(phone).matches()
    }

    private fun getUser(userId: Int){
        myUserFormViewModel.getUser(userId).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null){
                Toast.makeText(this, "User Loaded Successfully", Toast.LENGTH_SHORT).show()
                populateData(userDataResponse)
                pbMyUserFormLoading.visibility = View.GONE
                llMyUserFormContent.visibility = View.VISIBLE
                setFavorite()
            } else {
                Toast.makeText(this, "Failed to Load User", Toast.LENGTH_SHORT).show()
                pbMyUserFormLoading.visibility = View.GONE
            }
        })
    }

    private fun setFavorite() {
        if (userId != null) {
            favViewModel.getUser(userId as Int).observe(this, Observer { favUser ->
                Log.d("Isam ", "Get User: $favUser")
                isFavorite = favUser != null
                setFavoriteIcon()

                if (favUser != null) {
                    favoriteUserId = favUser.favUserId
                }
            })
        }
    }

    private fun createUserData(userData: UserDataResponse) {
        myUserFormViewModel.postUser(userData).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null) {
                Toast.makeText(this, "User Created Successfully", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "User Create Failed", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateUserData(id: Int, userData: UserDataResponse) {
        myUserFormViewModel.updateUser(id, userData).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null) {
                Toast.makeText(this, "User Updated Successfully", Toast.LENGTH_SHORT).show()
                updateFavoriteUser(userData)
                finish()
            } else {
                Toast.makeText(this, "User Updated Failed", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateFavoriteUser(userData: UserDataResponse) {
        if (favoriteUserId != null) {
            favViewModel.updateUser(
                FavoriteUser(
                    favoriteUserId as Long,
                    userData.address,
                    userData.id.toString(),
                    userData.name,
                    userData.phone
                )
            )
        }
    }

    private fun deleteUser() {
        pbMyUserFormLoading.visibility = View.VISIBLE
        myUserFormViewModel.deleteUser(userId as Int).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null) {
                Toast.makeText(this, "User Deleted Successfully", Toast.LENGTH_SHORT).show()
                if (isFavorite) removeFromFavorite()
                finish()
            } else {
                Toast.makeText(this, "User Delete Failed", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuItem = menu
        menuInflater.inflate(R.menu.menu_favorite, menu)
        setFavoriteIcon()

        if (!isEditUser){
            menu.findItem(R.id.actionFavorite).isVisible = false
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.actionFavorite -> {
                if (llMyUserFormContent.visibility == View.GONE){
                    Toast.makeText(this, "Can't Add to Favorite", Toast.LENGTH_SHORT).show()
                    return false
                }
                isFavorite = !isFavorite
                setFavoriteIcon()
                addOrRemoveFavorite()
                true
            }
            else -> true
        }
    }

    private fun setFavoriteIcon() {
        if (isFavorite){
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite)
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_unfavorite)
        }
    }

    private fun addOrRemoveFavorite() {
        if (isFavorite){
            addToFavorite()
        } else {
            removeFromFavorite()
        }

        favViewModel.getAllFavoriteUser().observe(this, Observer { listFavUser ->
            if (listFavUser.isNotEmpty()) {
                for (i in listFavUser.indices) {
                    Log.d("Isam ", "${listFavUser[i].favUserId} - ${listFavUser[i].userName}" )
                }
            }
        })
    }

    private fun addToFavorite() {
        favViewModel.insertUser(FavoriteUser(
            0L,
            etMyUserAddress.text.toString(),
            userId.toString(),
            etMyUserName.text.toString(),
            etMyUserPhone.text.toString()
        ))
    }

    private fun removeFromFavorite() {
        if (userId != null) {
            favViewModel.deleteUser(userId as Int)
        }
        favoriteUserId = null
    }
}
