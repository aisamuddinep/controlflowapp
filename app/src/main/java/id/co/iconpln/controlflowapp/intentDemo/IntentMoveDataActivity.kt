package id.co.iconpln.controlflowapp.intentDemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_intent_move_data.*

class IntentMoveDataActivity : AppCompatActivity() {

    private val tvIntentMoveDataReceive : TextView
        get() = tv_intent_move_data_receive

    companion object{
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_AGE = "extra_age"
    }

    private var name:String = ""
    private var age:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_move_data)

        getIntentExtra()
        showData()
    }

    private fun getIntentExtra() {
        name = intent.getStringExtra(EXTRA_NAME) ?: ""
        age = intent.getIntExtra(EXTRA_AGE, 0)
    }

    private fun showData() {
        val text = "Name: $name \nAge: $age"
        tvIntentMoveDataReceive.text = text
    }
}
