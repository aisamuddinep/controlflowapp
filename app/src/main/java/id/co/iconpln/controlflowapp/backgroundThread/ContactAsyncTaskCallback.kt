package id.co.iconpln.controlflowapp.backgroundThread

interface ContactAsyncTaskCallback {
    fun onPreExecute()
    fun onProgressUpdate(vararg values: Int?)
    fun onPostExecute(result: String?)
}