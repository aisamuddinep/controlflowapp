package id.co.iconpln.controlflowapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Hero (
    val name: String = "",
    val desc: String = "",
    val photo: String = ""
) : Parcelable