package id.co.iconpln.controlflowapp.fragments


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_option_dialog.*

/**
 * A simple [Fragment] subclass.
 */
class OptionDialogFragment : DialogFragment(), View.OnClickListener {

    companion object {
        val TAG = OptionDialogFragment::class.java.simpleName
    }

    private var optionDialogListener: OnOptionDialogListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_option_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListener()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val parentFragment = parentFragment
        if (parentFragment is LastFragment) {
            val lastFragment = parentFragment

            this.optionDialogListener = lastFragment.optionDialogListener
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.optionDialogListener = null
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnDialogClose -> {
                dialog?.cancel()
            }
            R.id.btnDialogChoose -> {
                val checkedRadioButtonId = rgDialogOption.checkedRadioButtonId
                if (checkedRadioButtonId != -1) {
                    val favColor = when (checkedRadioButtonId) {
                        R.id.rbDialogBlue -> rbDialogBlue.text.toString().trim()
                        R.id.rbDialogRed -> rbDialogRed.text.toString().trim()
                        R.id.rbDialogPurple -> rbDialogPurple.text.toString().trim()
                        R.id.rbDialogGreen -> rbDialogGreen.text.toString().trim()
                        else -> ""
                    }
                    Log.d(TAG, "color = $favColor")
                    if (optionDialogListener != null) {
                        optionDialogListener?.onOptionChosen(favColor)
                    }
                    dialog?.dismiss()
                }
            }
        }
    }

    private fun setOnClickListener() {
        btnDialogClose.setOnClickListener(this)
        btnDialogChoose.setOnClickListener(this)
    }

    interface OnOptionDialogListener {
        fun onOptionChosen(text: String)
    }

}
