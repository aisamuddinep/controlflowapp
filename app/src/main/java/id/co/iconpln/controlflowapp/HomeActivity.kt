package id.co.iconpln.controlflowapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.backgroundThread.BackgroundThreadActivity
import id.co.iconpln.controlflowapp.bottomSheetDialog.BottomSheetActivity
import id.co.iconpln.controlflowapp.constraint.ComplexConstraintActivity
import id.co.iconpln.controlflowapp.constraint.ConstraintActivity
import id.co.iconpln.controlflowapp.contact.ContactActivity
import id.co.iconpln.controlflowapp.contactFragment.ContactTabActivity
import id.co.iconpln.controlflowapp.fragmentBottomNav.BottomNavActivity
import id.co.iconpln.controlflowapp.fragmentNavDrawer.NavDrawerActivity
import id.co.iconpln.controlflowapp.fragmentTab.TabActivity
import id.co.iconpln.controlflowapp.fragmentViewPager.ScrollActivity
import id.co.iconpln.controlflowapp.fragments.DemoFragmentActivity
import id.co.iconpln.controlflowapp.hero.GridHeroActivity
import id.co.iconpln.controlflowapp.hero.ListHeroActivity
import id.co.iconpln.controlflowapp.intentDemo.IntentActivity
import id.co.iconpln.controlflowapp.myContact.MyContactActivity
import id.co.iconpln.controlflowapp.myProfile.MyProfileActivity
import id.co.iconpln.controlflowapp.myUser.MyUserActivity
import id.co.iconpln.controlflowapp.operation.OperationActivity
import id.co.iconpln.controlflowapp.sharedPreferences.SharedPreferencesActivity
import id.co.iconpln.controlflowapp.weather.WeatherActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnCalculation.setOnClickListener(this)
        btnClassification.setOnClickListener(this)
        btnLogin.setOnClickListener(this)
        btnHomeOperation.setOnClickListener(this)
        btnHomeStyle.setOnClickListener(this)
        btnHomeActivity.setOnClickListener(this)
        btnHomeVolume.setOnClickListener(this)
        btnHomeIntent.setOnClickListener(this)
        btnHomeConstraint.setOnClickListener(this)
        btnHomeComplexConstraint.setOnClickListener(this)
        btnHomeListHero.setOnClickListener(this)
        btnHomeGridHero.setOnClickListener(this)
        btnHomeDemoFragment.setOnClickListener(this)
        btnHomeTab.setOnClickListener(this)
        btnHomeBottomNav.setOnClickListener(this)
        btnHomeNavDrawer.setOnClickListener(this)
        btnHomeBottomSheet.setOnClickListener(this)
        btnHomeLocalization.setOnClickListener(this)
        btnHomeScroll.setOnClickListener(this)
        btnHomeSharedPreferences.setOnClickListener(this)
        btnHomeWeather.setOnClickListener(this)
        btnHomeContact.setOnClickListener(this)
        btnHomeBackgroundThread.setOnClickListener(this)
        btnHomeContactFragment.setOnClickListener(this)
        btnHomeMyContact.setOnClickListener(this)
        btnHomeMyUser.setOnClickListener(this)
        btnHomeMyProfileLogin.setOnClickListener(this)
    }

    override fun onClick(view: View) {

        when (view.id) {
            R.id.btnCalculation -> startIntent(MainActivity())
            R.id.btnClassification -> startIntent(ClassificationActivity())
            R.id.btnLogin -> startIntent(LoginActivity())
            R.id.btnHomeOperation -> startIntent(OperationActivity())
            R.id.btnHomeStyle -> startIntent(StyleActivity())
            R.id.btnHomeActivity -> startIntent(DemoActivity())
            R.id.btnHomeVolume -> startIntent(VolumeActivity())
            R.id.btnHomeIntent -> startIntent(IntentActivity())
            R.id.btnHomeConstraint -> startIntent(ConstraintActivity())
            R.id.btnHomeComplexConstraint -> startIntent(ComplexConstraintActivity())
            R.id.btnHomeListHero -> startIntent(ListHeroActivity())
            R.id.btnHomeGridHero -> startIntent(GridHeroActivity())
            R.id.btnHomeDemoFragment -> startIntent(DemoFragmentActivity())
            R.id.btnHomeTab -> startIntent(TabActivity())
            R.id.btnHomeBottomNav -> startIntent(BottomNavActivity())
            R.id.btnHomeNavDrawer -> startIntent(NavDrawerActivity())
            R.id.btnHomeBottomSheet -> startIntent(BottomSheetActivity())
            R.id.btnHomeLocalization -> startIntent(LocalizationActivity())
            R.id.btnHomeScroll -> startIntent(ScrollActivity())
            R.id.btnHomeSharedPreferences -> startIntent(SharedPreferencesActivity())
            R.id.btnHomeWeather -> startIntent(WeatherActivity())
            R.id.btnHomeContact -> startIntent(ContactActivity())
            R.id.btnHomeBackgroundThread-> startIntent(BackgroundThreadActivity())
            R.id.btnHomeContactFragment-> startIntent(ContactTabActivity())
            R.id.btnHomeMyContact-> startIntent(MyContactActivity())
            R.id.btnHomeMyUser-> startIntent(MyUserActivity())
            R.id.btnHomeMyProfileLogin-> startIntent(MyProfileActivity())
        }
    }

    private fun startIntent(activity: Activity) {
        val intent = Intent(this, activity::class.java)
        startActivity(intent)
    }

}
