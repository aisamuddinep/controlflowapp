package id.co.iconpln.controlflowapp.operation

import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.operation.Operation
import java.text.DecimalFormat

class OperationViewModel : ViewModel(){

    var operationResult: Float = 0f
    var operator:String = ""
    var msg: ArrayList<String> = ArrayList()
    val df:DecimalFormat = DecimalFormat("###.###")
    var divideByZero: Boolean = false

    fun execute(x: Float, operation: Operation) {
        operationResult = when (operation) {
            is Operation.Add -> operation.value + x
            is Operation.Subtract -> operation.value - x
            is Operation.Multiply -> operation.value * x
            is Operation.Divide -> operation.value / x
        }
    }
}