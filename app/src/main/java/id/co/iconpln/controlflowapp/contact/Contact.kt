package id.co.iconpln.controlflowapp.contact

data class Contact (
    var id: String = "0",
    var nama: String? = null,
    var email: String? = null,
    var mobile: String? = null
)