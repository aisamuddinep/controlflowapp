package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvMainResult.text = "Hasilnya: "
        btnMainShow.setOnClickListener{
            //Dikasih operasi perkalian
            if (etMainNilai.text.isNotEmpty()) {
                val text = etMainNilai.text.toString().toDouble()
                hitungPangkat(text)
            }else{
                val alert = AlertDialog.Builder(this)
                alert.setMessage("Please input number")

                alert.setPositiveButton(android.R.string.yes) { _, _ ->
                    Toast.makeText(applicationContext,
                        android.R.string.yes, Toast.LENGTH_SHORT).show()
                }
                alert.show()
            }
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    fun hitungPangkat(value: Double): Unit {
        val square = value.pow(2).toInt().toString()
        tvMainResult.text = "Hasilnya: $square"
    }
}
