package id.co.iconpln.controlflowapp.operation

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_operation.*

class OperationActivity : AppCompatActivity(), View.OnClickListener {

    private var inputX: Float = 0f
    private var inputY: Float = 0f
    private lateinit var operationViewModel: OperationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initViewModel()
        displayResult()
        setButtonClickListener()
    }

    private fun initViewModel() {
        operationViewModel = ViewModelProviders.of(this).get(OperationViewModel::class.java)
    }

    private fun displayResult() {
        if (operationViewModel.msg.isNotEmpty()) {
            tvOperationResult.text = getString(R.string.error)
            tvOperationResult.setTextColor(ContextCompat.getColor(this, android.R.color.holo_red_dark))
        } else {
            tvOperationResult.text =
                operationViewModel.df.format(operationViewModel.operationResult)
            tvOperationResult.setTextColor(ContextCompat.getColor(this,
                R.color.colorPrimaryDark
            ))
        }
        tvOperator.text = operationViewModel.operator
    }

    private fun resetView() {
        etBilanganX.setText(getString(R.string.operation_zero))
        etBilanganY.setText(getString(R.string.operation_zero))
        operationViewModel.operationResult = 0f
        operationViewModel.operator = ""
        operationViewModel.divideByZero = false
        operationViewModel.msg.clear()
    }

    private fun getInputNumber() {
        if (etBilanganX.text?.isNotEmpty() == true
            && etBilanganY.text?.isNotEmpty() == true
        ) {
            inputX = etBilanganX.text.toString().toFloat()
            inputY = etBilanganY.text.toString().toFloat()
        } else {
            if (etBilanganX.text?.isEmpty() == true) {
                operationViewModel.msg.add(getString(R.string.warning_bilangan_x))
            }
            if (etBilanganY.text?.isEmpty() == true) {
                operationViewModel.msg.add(getString(R.string.warning_bilangan_y))
            }
        }
    }

    private fun setButtonClickListener() {
        btnOpAdd.setOnClickListener(this)
        btnOpSubtract.setOnClickListener(this)
        btnOpMultiply.setOnClickListener(this)
        btnOpDivide.setOnClickListener(this)
        btnOperation.setOnClickListener(this)
        btnReset.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnOpAdd -> {
                operationViewModel.operator = getString(R.string.operation_add)
            }
            R.id.btnOpSubtract -> {
                operationViewModel.operator = getString(R.string.operation_subtract)
            }
            R.id.btnOpMultiply -> {
                operationViewModel.operator = getString(R.string.operation_multiply)
            }
            R.id.btnOpDivide -> {
                operationViewModel.operator = getString(R.string.operation_divide)
            }
            R.id.btnOperation -> {
                calculateOperation()
            }
            R.id.btnReset -> {
                resetView()
            }
        }
        displayResult()
        showError(operationViewModel.msg)
    }

    private fun calculateOperation() {
        getInputNumber()
        operationViewModel.divideByZero =
            operationViewModel.operator == getString(R.string.operation_divide) && inputY == 0f && etBilanganY.text?.isNotBlank() == true
        when {
            operationViewModel.divideByZero -> {
                operationViewModel.msg.add(getString(R.string.operation_divide_zero))
            }
            operationViewModel.operator == "" -> {
                operationViewModel.msg.add(getString(R.string.operation_warning_select_first))
            }
            operationViewModel.msg.isEmpty() -> {
                val operation = createOperator()
                operationViewModel.execute(inputY, operation as Operation)
            }
        }
    }

    private fun showError(msg: ArrayList<String>) {
        if (msg.isNotEmpty()) {
            val toast = Toast.makeText(
                this,
                msg.joinToString(separator = "\n"),
                Toast.LENGTH_SHORT
            )
            val layout = toast.view as LinearLayout
            if (layout.childCount > 0) {
                val tv = layout.getChildAt(0) as TextView
                tv.gravity = Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL
            }
            toast.show()
            operationViewModel.msg.clear()
        }
    }

    private fun createOperator(): Operation? {
        return when (operationViewModel.operator) {
            getString(R.string.operation_add) -> Operation.Add(inputX)
            getString(R.string.operation_subtract) -> Operation.Subtract(inputX)
            getString(R.string.operation_multiply) -> Operation.Multiply(inputX)
            getString(R.string.operation_divide) -> Operation.Divide(inputX)
            else -> null
        }
    }

}
